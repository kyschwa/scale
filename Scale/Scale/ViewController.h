//
//  ViewController.h
//  Scale
//
//  Created by Kyle Schwarzkopf on 7/20/15.
//  Copyright © 2015 nil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController {
    
    IBOutlet UIButton *ScaleButton;
    IBOutlet UIButton *Homescreen;
    IBOutlet UIButton *ResetButton;
    IBOutlet UILabel *IdiotLabel;
    IBOutlet UILabel *GramScale;
    
    NSTimer *timer;
    double r;
}

@end

