//
//  AppDelegate.h
//  Scale
//
//  Created by Kyle Schwarzkopf on 7/20/15.
//  Copyright © 2015 nil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

