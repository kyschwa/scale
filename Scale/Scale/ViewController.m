//
//  ViewController.m
//  Scale
//
//  Created by Kyle Schwarzkopf on 7/20/15.
//  Copyright © 2015 nil. All rights reserved.
//

#import "ViewController.h"

#define ARC4RANDOM_MAX      0x100000000

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ResetButton.hidden = YES;
    IdiotLabel.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)ScaleButton:(id)sender
{
    NSLog(@"pressed");
    timer = [NSTimer scheduledTimerWithTimeInterval:0.25 target:self selector:@selector(randomNumber) userInfo:nil repeats:NO];
    timer = [NSTimer scheduledTimerWithTimeInterval:0.50 target:self selector:@selector(randomNumber) userInfo:nil repeats:NO];
    timer = [NSTimer scheduledTimerWithTimeInterval:0.75 target:self selector:@selector(randomNumber) userInfo:nil repeats:NO];
    timer = [NSTimer scheduledTimerWithTimeInterval:1.00 target:self selector:@selector(randomNumber) userInfo:nil repeats:NO];
    timer = [NSTimer scheduledTimerWithTimeInterval:1.25 target:self selector:@selector(randomNumber) userInfo:nil repeats:NO];
    timer = [NSTimer scheduledTimerWithTimeInterval:1.50 target:self selector:@selector(randomNumber) userInfo:nil repeats:NO];
    timer = [NSTimer scheduledTimerWithTimeInterval:1.75 target:self selector:@selector(randomNumber) userInfo:nil repeats:NO];
    timer = [NSTimer scheduledTimerWithTimeInterval:2.00 target:self selector:@selector(randomNumber) userInfo:nil repeats:NO];
    timer = [NSTimer scheduledTimerWithTimeInterval:2.25 target:self selector:@selector(randomNumber) userInfo:nil repeats:NO];
    timer = [NSTimer scheduledTimerWithTimeInterval:2.50 target:self selector:@selector(randomNumber) userInfo:nil repeats:NO];
    timer = [NSTimer scheduledTimerWithTimeInterval:2.75 target:self selector:@selector(randomNumber) userInfo:nil repeats:NO];
    timer = [NSTimer scheduledTimerWithTimeInterval:3.00 target:self selector:@selector(scaleFiller) userInfo:nil repeats:NO];
    
    ScaleButton.hidden = YES;
    ResetButton.hidden = NO;
    
    
}

-(void)scaleFiller
{
    
    GramScale.text = [NSString stringWithFormat:@"You're an idiot"];
    IdiotLabel.hidden = NO;
    [timer invalidate];
    
}


-(void)randomNumber
{

    r = ((double)arc4random() / ARC4RANDOM_MAX) ;
    GramScale.text = [NSString stringWithFormat:@"%.02fgm", r];
    
}


-(IBAction)ResetButton:(id)sender
{
    
    ResetButton.hidden = YES;
    ScaleButton.hidden = NO;
    IdiotLabel.hidden = YES;
    GramScale.text = [NSString stringWithFormat:@"0:00 gm"];
    
}


@end
